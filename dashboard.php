<?php
session_start();


if (!isset($_SESSION['email'])) {
    header("Location: index.php");
}



 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <?php 
            if(isset($_GET['success'])){
                 echo   "<div class='alert alert-success text-center' role='alert'>
                                Successfully submited to HR Department
                         </div>";
            }
         ?>
        <div class="row mt-5 mx-auto">
            <div class="col">
                <a href="myantel.php" class="btn btn-block"><img src="./image/myantelLogo.jpeg" class="img-fluid" style="width : 50%; border : 1px solid #000; border-radius : 20px" alt="Responsive image"></a>
            </div>
            <div class="col">
            <a href="solutionhub.php" class="btn btn-block"><img src="./image/solutionHubLogo.jpeg" class="img-fluid" style="width : 50%; border : 1px solid #000;border-radius : 20px" alt="Responsive image"></a>
            </div> 
        </div>
    <?php
        if(isset($_SESSION['role'])){

            if($_SESSION['role'] === "hr"){
    ?>
            <div class="row justify-content-center mt-5">
            <div class="col-4">
                <a href="hr.php" class="btn btn-primary btn-block">Go To HR Dashboard</a>
            </div>
        </div>
    <?php
            }
        }
    ?>
        


        <div class="row justify-content-center mt-5">
            <div class="col-4">
                <a href="logout.php" class="btn btn-danger btn-block">Logout</a>
            </div>
        </div>
    </div>
    
</body>
</html>


