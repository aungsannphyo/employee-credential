<?php

session_start();
include_once("./classes/solutionhub.php");

$myantel  = new MyanTel(); 

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){

    $firstEmployeeName = $_POST['firstEmployeeName'];
    $secondEmployeeName = $_POST['secondEmployeeName'];
    $thirdEmployeeName = $_POST['thirdEmployeeName'];
    $nrc = $_POST['nrc'];
    $date = $_POST['date'];

    $result = $myantel->submitHandler($firstEmployeeName, $secondEmployeeName, $thirdEmployeeName, $nrc, $date);
}

if (!isset($_SESSION['email'])) {
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Solution Hub</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body">

    <div class="container">

    <?php
        if(isset($result)){

            if($result === 400) {
            echo "<div class='alert alert-danger text-center' role='alert'>
                All of the input box must not be empty!
            </div>";
            }else if($result === 200) {
                echo "<div class='alert alert-success text-center' role='alert'>
                    Successfully submited to HR
                </div>";
            } else if($result === 422) {
                echo "<div class='alert alert-danger text-center' role='alert'>
                    Something went wrong.Please try again later
                </div>";
            }
        }
    ?>

    <!-- Header -->
        <div class="row">
            <div class="col">
            <img src="./image/SolutionHub.png" class="img-fluid" alt="Responsive image">
            </div>
        </div>
       
        <!-- Employee Confidentiality Agreement         -->
        <div class="row">
            <div class="col">

                <h4 class="text-center mb-5"><u>Employee Confidentiality Agreement</u></h4>

                <p>This Confidentiality (“Agreement”) is entered into between _____________________ (“Employer”) and </p>
                
                <form action="" method="POST">     
                    <div class="form-group mb-5">
                        <label for="exampleInputEmail1">Employee</label>
                        <input type="text" class="form-control" id="employee"  name="firstEmployeeName" aria-describedby="employee" placeholder="Your Name">
                    </div>
                   

                <p><strong>1.  CONFIDENTIAL INFORMATION</strong></p>

                <p>The Employee understands that the Confidential Information and Proprietary Data are trade secrets of the Employer and must always take reasonable steps in order to protect the confidentiality of said information.</p>

                <p>The Employee agrees not to use any Confidential Information or Proprietary Data for their personal benefit or for the benefit of others during their employment.</p>

                <p>The Employee agrees Confidential Information and Proprietary Data is the exclusive property of the Employer and will not remove it from the premises of the Employer under any circumstances, unless granted prior written approval by the Employer. If it is removed, then upon any termination, the Employee must return the information and data and must not make any copies.</p>

                <p>Employees are responsible for company property entrusted to them. This property includes, but is not limited to physical property (such as phones, company vehicles or computers), records (such as data on customers and company records), and intangible property (such as computer software and computer records). It is important that, whichever category the property falls into, employees must treat the Group’s property as they would their own and must not damage it, deface it or remove it for personal use, unless authorized to do so.</p>

                <p>Similarly, they are responsible for the proper expenditure of the company’s funds including expenses. While spending or committing company funds, employees must be sure that the transaction is proper and documented, and that the company receives appropriate value in return.</p>

                <p>The Employee agrees that he or she will not disclose to any person or entity, either directly or indirectly, the Confidential Information or Proprietary Data at any time. Any use or disclosure of Confidential Information or Proprietary Data is cause for an action by the court of the State or a federal court.</p>

                <p><strong>2.  PROPRIETARY INFORMATION AND INTELLECTUAL PROPERTY</strong></p>

                <p>Many employees have access to information, which includes the trade secrets, know-how used by the Group to distinguish its businesses and services from those of competitors, as well as sensitive private business information of a commercial, technical  or  financial  nature  such  as  prospects,  agreements  with  customers,  business  partners,  competitors,  account plans, business proposals, negotiations and contracts.</p>

                <p>It is important that all company proprietary information is kept confidential. Employees have a duty to safeguard company information, bearing in mind ethical, legal ramifications and government regulations. Information of commercial value or of a sensitive nature must be tightly controlled. For example, when releasing information to a third party for a bid proposal, a Non-Disclosure Agreement (format provided by legal department) should be signed by the third parties, and information released is on a need-to-know basis.</p>

                <p>Any trademark, copyrights, patents, designs, registered designs, proprietary information and all other intellectual property rights developed and commissioned by the company belong to the Group. Employees are reminded not to infringe any third parties' rights including, but not limited to, any third-party intellectual property rights, copyrights, patents and trademarks.</p>

                <p>The Group will hold exclusive property of any invention, discovery, design or improvements made. This could also include inventions employees may create which relate to the company’s business, regardless of whether the invention or designs are patentable or are capable of being registered or copyrighted.</p>

                <p>Employees must report these inventions to the company and shall, at the company's request and expense, disclose information relating to the invention and do what is required to obtain patents or industrial rights relating to the invention. The patents will be in the name of the company or its nominee and the employee will not be entitled to any payment for the invention.</p>

                <p><strong>3. RETURN OF INFORMATION</strong></p>   

                <p>The company's ownership of any intellectual property created by employees while with the company continues after they have left service.</p>

                <p>When employees leave the company for any reason, including retirement, they must return all the company’s property, including all documents and records in their possession, and they must not disclose or misuse company confidential information at any time. Employees are also responsible for protecting information provided in confidence by any third party, such as a customer, supplier or a partner, after they leave the company.</p>

                <p><strong>4. CONFLICT OF INTEREST</strong></p>

                <p>A ‘conflict of interest’ arises when employees have a competing professional or personal interest that would either make it difficult to fulfil their duties properly, or would create an appearance of impropriety that could undermine customer or public confidence.</p>

                <p>Employees must do nothing that conflicts with the interests of the Group, or anything that could be construed as being in conflict, for example, participating in the evaluation/approval of award to a vendor in which an employee has a vested interest (either personally, or through close relatives).  Employees should declare/disqualify themselves from handling transactions which put them, whether perceived or real, in a position of conflict.</p>

                <p>Employees must avoid all situations which could result in conflicts of interest. They should comply with reporting and disclosure requirements of potential or actual conflicts of interest, and disclose any matters which could reasonably be expected to interfere with their professional duties.</p>

                <p><strong>5. TRADE SECRETS</strong></p>

                <p>Employee shall not at any time, or in any manner, either directly or indirectly, disclose or communicate to any person, firm, cooperation, or other entity in any manner whatsoever any information concerning any matters affecting or relating to the business of employer, including but not limited to any of its customer, the prices it obtains or has obtained from the sale of, or at which it sells or has sold, its products, or any other information concerning the business of employer, its manner of operation, its plan, processes, or other data without regard to whether all of the above-stated matters will be deemed confidential, material, or important, employer and employee specifically and expressly stipulating that as between them, such matters are important, material, confidential and gravely affect the effective and successful conduct of the business of employer, and employer’s good will, and that any breach of the terms of this section shall be a material breach of this Agreement.</p>

                <p><strong>6. BUSINESS DEALING</strong></p>

                <p>Employees should not engage in any outside business dealings that involve or could appear to involve, a conflict between their personal interests and the interests of the Group (i.e. conflict of interests).</p>

                <p>Employees must not have any direct or indirect financial or business interest in or dealings with competitors, suppliers, customers or anyone else with whom they are engaged in a business relationship on behalf of the Group, which might or might appear to, create a conflict of interest, or impair the judgments they make on behalf of the Group.</p>

                <p>They should also not engage in any personal business dealings which detract from or conflict with their employment in [Company].</p>

                <p>Employees must avoid situations where their loyalties may be divided between the Group's interest and those of a customer, supplier or competitor.</p>

                <p>Employees must not take advantage of any opportunity for personal gain that rightfully belongs to [Company]. They should avoid putting themselves in any situation which might, or might appear to put them at a personal advantage, and they must report any potentially compromising situation to their supervisors promptly.</p>

                <p><strong>7. NATURE OF RELATIONSHIP</strong></p>

                <p>It is agreed that this Agreement does not define the terms of the contract, nor does this Agreement guarantee the continuation of employment between the Employer and Employee. Both parties understand that the Employee’s relationship with the Employer is terminable “at will,” therefore either Employer or Employee has the right to terminate the relationship with or without cause or even prior notice.
                </p>

                <p> This Agreement comprises the entire agreement between the Employer and the Employee in relation to the subject matter within and supersedes any previous agreements between both parties in relation to confidentiality.</p>

                <p class="mb-3">This Agreement is effective as of the date written below:</p>

                <div class="row mb-5">
                    <div class="col">Executed by “The Employer”, _____________ by or in the presence of:</div>
                    <div style="
                        border-left: 3px solid black; 
                        height: 530px; 
                        position:absolute; 
                        left: 50%; "
                    ></div> 
                    <div class="col">
                        Executed by “The Employee”,   
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" name="secondEmployeeName" id="employee" aria-describedby="employee" placeholder="Your Name">
                        </div> 
                        by or in the presence of:
                    </div>
                </div>
                <div style="border : 1px solid black;"></div>
                <div class="row mt-5">
                    <div class="col"> 
                        <p class="mb-3">Signature</p> </br>
                        Name: __________________ </br>
                        Position: ________________ </br>
                        Date: _________________
                    </div>
                     
                    <div class="col"> 
                        <p class="mb-3">Signature</p> </br>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" name="thirdEmployeeName" id="employee" aria-describedby="employee" placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">NRC</label>
                            <input type="text" class="form-control" name="nrc" id="employee" aria-describedby="employee" placeholder="Eg : 9/MAYATA(N)123456">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date</label>
                            <input type="date" class="form-control" name="date" id="employee" aria-describedby="employee" placeholder="Your Name">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center mt-5">
                    <div class="col-6">
                        <button type="submit" name="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
                
            </div>
            </form>
        </div>
        <!-- Footer -->
        <div class="row">
            <div class="col">
            <img src="./image/SolutionHub-footer.png" class="img-fluid" alt="Responsive image">
            </div>
        </div>
    </div>
    
</body>
</html>

